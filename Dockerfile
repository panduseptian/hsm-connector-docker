FROM node

RUN apt-get update -y
RUN apt-get install opensc python libcap2 kmod -y

# RUN pkcs11-tool

COPY ./installer /temp/installer

RUN tar -C / -xf /temp/installer/ctd.tar.gz
RUN tar -C / -xf /temp/installer/ctls.tar.gz
RUN tar -C / -xf /temp/installer/devref.tar.gz
RUN tar -C / -xf /temp/installer/hwsp.tar.gz
RUN tar -C / -xf /temp/installer/javasp.tar.gz
RUN tar -C / -xf /temp/installer/jd.tar.gz
RUN tar -C / -xf /temp/installer/ncsnmp.tar.gz
RUN tar -C / -xf /temp/installer/raserv.tar.gz

RUN cd /opt/nfast/sbin/ && ls && ./install

RUN rm -rf /temp/installer

CMD /etc/init.d/nc_hardserver start && pkcs11-tool --module "/opt/nfast/toolkits/pkcs11/libcknfast.so" -l -O --slot 1